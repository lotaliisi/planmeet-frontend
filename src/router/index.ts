import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import MeetingsIndex from '../views/User/Meetings/index.vue';
import MeetingDetails from '../views/User/Meetings/details.vue';
import MeetingCreate from '../views/User/Meetings/create.vue';
import MeetingEdit from '../views/User/Meetings/edit.vue';
import AccountLogin from '../views/Account/Login.vue';
import QuestionnairesIndex from '../views/User/Questionnaires/index.vue';
import QuestionnaireDetails from '../views/User/Questionnaires/details.vue';
import QuestionnaireEdit from '../views/User/Questionnaires/edit.vue';
import QuestionnaireCreate from '../views/User/Questionnaires/create.vue';
import OrganizationsIndex from '../views/Admin/Organizations/index.vue';
import OrganizationEdit from '../views/Admin/Organizations/edit.vue';
import OrganizationCreate from '../views/Admin/Organizations/create.vue';
import HolidaysIndex from '../views/Admin/Holidays/index.vue';
import HolidayEdit from '../views/Admin/Holidays/edit.vue';
import HolidayCreate from '../views/Admin/Holidays/create.vue';
import UsersIndex from '../views/Admin/Users/index.vue';
import UserEdit from '../views/Admin/Users/edit.vue';
import UserCreate from '../views/Admin/Users/create.vue';
import FindRooms from '../views/User/TalTechRooms/find.vue';
import BookRoom from '../views/User/TalTechRooms/book.vue';
import { ERole } from '@/domain/ERole';

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    { path: '/', name: 'AccountLogin', component: AccountLogin },
    { path: '/meetings', name: 'Meetings', component: MeetingsIndex, meta: { authorize: ERole.User } },
    { path: '/meetings/:id?', name: 'MeetingDetails', component: MeetingDetails, props: true, meta: { authorize: ERole.User } },
    { path: '/meetings/create', name: 'MeetingCreate', component: MeetingCreate, meta: { authorize: ERole.User } },
    { path: '/meetings/edit/:id?', name: 'MeetingEdit', component: MeetingEdit, props: true, meta: { authorize: ERole.User } },
    { path: '/questionnaires', name: 'Questionnaires', component: QuestionnairesIndex, meta: { authorize: ERole.User } },
    { path: '/questionnaires/:id?', name: 'QuestionnaireDetails', component: QuestionnaireDetails, props: true, meta: { authorize: ERole.User } },
    { path: '/questionnaires/edit/:id?', name: 'QuestionnaireEdit', component: QuestionnaireEdit, props: true, meta: { authorize: ERole.User } },
    { path: '/questionnaires/create/:meetingId?', name: 'QuestionnaireCreate', component: QuestionnaireCreate, props: true, meta: { authorize: ERole.User } },
    { path: '/organizations', name: 'Organizations', component: OrganizationsIndex, meta: { authorize: ERole.Admin } },
    { path: '/organizations/:id?', name: 'OrganizationEdit', component: OrganizationEdit, props: true, meta: { Adminauthorize: ERole.Admin } },
    { path: '/organizations/create', name: 'OrganizationCreate', component: OrganizationCreate, meta: { authorize: ERole.Admin } },
    { path: '/holidays', name: 'Holidays', component: HolidaysIndex, meta: { authorize: ERole.Admin } },
    { path: '/holidays/:id?', name: 'HolidayEdit', component: HolidayEdit, props: true, meta: { authorize: ERole.Admin } },
    { path: '/holidays/create', name: 'HolidayCreate', component: HolidayCreate, meta: { authorize: ERole.Admin } },
    { path: '/users', name: 'Users', component: UsersIndex, meta: { authorize: ERole.Admin } },
    { path: '/users/:id?', name: 'UserEdit', component: UserEdit, props: true, meta: { authorize: ERole.Admin } },
    { path: '/users/create', name: 'UserCreate', component: UserCreate, meta: { authorize: ERole.Admin } },
    { path: '/rooms/:meetingId?', name: 'FindRooms', component: FindRooms, props: true, meta: { authorize: ERole.User } },
    { path: '/rooms/book/:id?/:meetingId?', name: 'BookRoom', component: BookRoom, props: true, meta: { authorize: ERole.User } }
]

const router = new VueRouter({
    routes
})

export default router

router.beforeEach((to, from, next) => {
    const { authorize } = to.meta;
    if (authorize) {
        if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
            if (authorize === ERole.User && sessionStorage.getItem('user') === "true") {
                return next();
            } else if (authorize === ERole.Admin && sessionStorage.getItem('admin') === "true") {
                return next();
            } else {
                return next(false);
            }
        } else {
            return next({ name: 'AccountLogin' });
        }
    }
    return next();
})
