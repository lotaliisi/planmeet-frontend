export interface ILoginResponseDTO {
    token: string;
    type: string;
    id: number;
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    birthday: Date;
    roles: string[];
}
