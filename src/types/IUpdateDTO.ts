export interface IUpdateDTO {
    id: number;
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    birthday: string;
    password: string;
    roles: string[];
}

