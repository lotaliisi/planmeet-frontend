import Vue from 'vue'
import Vuex from 'vuex'
import { ILoginDTO } from '@/types/ILoginDTO';
import { AccountApi } from '@/services/AccountApi';
import { IMeeting, IMeetingCreate } from '@/domain/IMeeting';
import { IBuilding, IBuildingCreate } from '@/domain/IBuilding';
import { MeetingApi } from '@/services/MeetingApi';
import { IOrganization, IOrganizationCreate } from '@/domain/IOrganization';
import { OrganizationApi } from '@/services/OrganizationsApi';
import { IHoliday, IHolidayCreate } from '@/domain/IHoliday';
import { HolidayApi } from '@/services/HolidayApi';
import { IOrganizationHoliday, IOrganizationHolidayCreate } from '@/domain/IOrganizationHoliday';
import { OrganizationHolidayApi } from '@/services/OrganizationHolidayApi';
import { IQuestionnaire, IQuestionnaireCreate } from '@/domain/IQuestionnaire';
import { QuestionnaireApi } from '@/services/QuestionnaireApi';
import { MeetingTimeApi } from '@/services/MeetingTimeApi';
import { IMeetingTime, IMeetingTimeCreate } from '@/domain/IMeetingTime';
import { IAnswer, IAnswerCreate } from '@/domain/IAnswer';
import { AnswerApi } from '@/services/AnswerApi';
import { IMemberAnswer, IMemberAnswerCreate } from '@/domain/IMemberAnswer';
import { MemberAnswerApi } from '@/services/MemberAnswerApi';
import { IEquipment, IEquipmentCreate } from '@/domain/IEquipment';
import { BuildingApi } from '@/services/BuildingApi';
import { EquipmentApi } from '@/services/EquipmentApi';
import { TalTechRoomsApi } from '@/services/TalTechRoomsApi';
import { ITalTechRoomsQuery } from '@/domain/ITalTechRoomsQuery';
import { ITalTechRoomsResponse } from '@/domain/ITalTechRoomsResponse';
import { IUser } from '@/domain/IUser';
import { UserApi } from '@/services/UserApi';
import { ILoginResponseDTO } from '@/types/ILoginResponseDTO';
import { ISignupDTO } from '@/types/ISignupDTO';
import { UserOrganizationApi } from '@/services/UserOrganizationApi';
import { IUserOrganization } from '@/domain/IUserOrganization';
import { IRole } from '@/domain/IRole';
import { RoleApi } from '@/services/RoleApi';
import { IBookType } from '@/domain/IBookType';
import { BookTypeApi } from '@/services/BookTypeApi';
import { ITalTechRoomsBookQuery } from '@/domain/ITalTechRoomsBookQuery';
import { IUpdateDTO } from '@/types/IUpdateDTO';
import { IMeetingMember, IMeetingMemberCreate } from '@/domain/IMeetingMember';
import { MeetingMemberApi } from '@/services/MeetingMemberApi';
import { ERole } from '@/domain/ERole';
import { ITalTechRoom, ITalTechRoomCreate } from '@/domain/ITalTechRoom';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        users: [] as IUser[],
        user: null as IUser | null,
        roles: [] as IRole[],
        role: null as IRole | null,
        meetings: [] as IMeeting[],
        meeting: null as IMeeting | null,
        organizations: [] as IOrganization[],
        organization: null as IOrganization | null,
        holidays: [] as IHoliday[],
        holiday: null as IHoliday | null,
        organizationHolidays: [] as IOrganizationHoliday[],
        organizationHolidaysByHoliday: [] as IOrganizationHoliday[],
        organizationHolidaysByOrganization: [] as IOrganizationHoliday[],
        organizationHoliday: null as IOrganizationHoliday | null,
        questionnaires: [] as IQuestionnaire[],
        questionnaire: null as IQuestionnaire | null,
        meetingTimes: [] as IMeetingTime[],
        meetingTime: null as IMeetingTime | null,
        answers: [] as IAnswer[],
        answer: null as IAnswer | null,
        memberAnswers: [] as IMemberAnswer[],
        memberAnswer: null as IMemberAnswer | null,
        buildings: [] as IBuilding[],
        building: null as IBuilding | null,
        equipments: [] as IEquipment[],
        equipment: null as IEquipment | null,
        talTechRoomResponses: [] as ITalTechRoomsResponse[],
        talTechRoomResponse: null as ITalTechRoomsResponse | null,
        talTechRooms: [] as ITalTechRoom[],
        talTechRoom: null as ITalTechRoom | null,
        meetingInCreation: null as IMeetingCreate | null,
        questionnaireInCreation: null as IQuestionnaireCreate | null,
        answersInCreation: [] as string[],
        meetingTimesInCreation: [] as string[],
        userOrganizations: [] as IUserOrganization[],
        userOrganization: null as IUserOrganization | null,
        bookTypes: [] as IBookType[],
        bookType: null as IBookType | null,
        times: [] as string[],
        meetingMembers: [] as IMeetingMember[],
        meetingMember: null as IMeetingMember | null
    },
    mutations: {
        setUser(state, user: IUser) {
            state.user = user;
        },
        setUsers(state, users: IUser[]) {
            state.users = users;
        },
        setRole(state, role: IRole) {
            state.role = role;
        },
        setRoles(state, roles: IRole[]) {
            state.roles = roles;
        },
        setMeetings(state, meetings: IMeeting[]) {
            state.meetings = meetings;
        },
        setMeeting(state, meeting: IMeeting) {
            state.meeting = meeting;
        },
        setOrganizations(state, organizations: IOrganization[]) {
            state.organizations = organizations;
        },
        setOrganization(state, organization: IOrganization) {
            state.organization = organization;
        },
        setHolidays(state, holidays: IHoliday[]) {
            state.holidays = holidays;
        },
        setHoliday(state, holiday: IHoliday) {
            state.holiday = holiday;
        },
        setOrganizationHolidays(state, organizationHolidays: IOrganizationHoliday[]) {
            state.organizationHolidays = organizationHolidays;
        },
        setOrganizationHolidaysByHoliday(state, organizationHolidays: IOrganizationHoliday[]) {
            state.organizationHolidaysByHoliday = organizationHolidays;
        },
        setOrganizationHolidaysByOrganization(state, organizationHolidays: IOrganizationHoliday[]) {
            state.organizationHolidaysByOrganization = organizationHolidays;
        },
        setOrganizationHoliday(state, organizationHoliday: IOrganizationHoliday) {
            state.organizationHoliday = organizationHoliday;
        },
        setQuestionnaires(state, questionnaires: IQuestionnaire[]) {
            state.questionnaires = questionnaires;
        },
        setQuestionnaire(state, questionnaire: IQuestionnaire) {
            state.questionnaire = questionnaire;
        },
        setMeetingTimes(state, meetingTimes: IMeetingTime[]) {
            state.meetingTimes = meetingTimes;
        },
        setMeetingTime(state, meetingTime: IMeetingTime) {
            state.meetingTime = meetingTime;
        },
        setAnswers(state, answers: IAnswer[]) {
            state.answers = answers;
        },
        setAnswer(state, answer: IAnswer) {
            state.answer = answer;
        },
        setMemberAnswers(state, memberAnswers: IMemberAnswer[]) {
            state.memberAnswers = memberAnswers;
        },
        setMemberAnswer(state, memberAnswer: IMemberAnswer) {
            state.memberAnswer = memberAnswer;
        },
        setBuildings(state, buildings: IBuilding[]) {
            state.buildings = buildings;
        },
        setBuilding(state, building: IBuilding) {
            state.building = building;
        },
        setEquipments(state, equipments: IEquipment[]) {
            state.equipments = equipments;
        },
        setEquipment(state, equipment: IEquipment) {
            state.equipment = equipment;
        },
        setTalTechRoomResponses(state, talTechRooms: ITalTechRoomsResponse[]) {
            state.talTechRoomResponses = talTechRooms;
        },
        setTalTechRoomResponse(state, talTechRoom: ITalTechRoomsResponse) {
            state.talTechRoomResponse = talTechRoom;
        },
        setTalTechRooms(state, talTechRooms: ITalTechRoom[]) {
            state.talTechRooms = talTechRooms;
        },
        setTalTechRoom(state, talTechRoom: ITalTechRoom) {
            state.talTechRoom = talTechRoom;
        },
        setMeetingInCreation(state, meetinginCreation: IMeetingCreate) {
            state.meetingInCreation = meetinginCreation;
        },
        setUserOrganizations(state, userOrganizations: IUserOrganization[]) {
            state.userOrganizations = userOrganizations;
        },
        setUserOrganization(state, userOrganization: IUserOrganization) {
            state.userOrganization = userOrganization;
        },
        setBookTypes(state, bookTypes: IBookType[]) {
            state.bookTypes = bookTypes;
        },
        setBookType(state, bookType: IBookType) {
            state.bookType = bookType;
        },
        setMeetingMembers(state, meetingMembers: IMeetingMember[]) {
            state.meetingMembers = meetingMembers;
        },
        setMeetingMember(state, meetingMember: IMeetingMember) {
            state.meetingMember = meetingMember;
        }
    },
    getters: {
    },
    actions: {
        clearSessionStorage(): void {
            sessionStorage.removeItem('token');
            sessionStorage.removeItem('user');
            sessionStorage.removeItem('admin');
            sessionStorage.removeItem('username');
            sessionStorage.removeItem('id');
        },
        async authenticateUser(context, loginDTO: ILoginDTO): Promise<ILoginResponseDTO|null> {
            const user = await AccountApi.signin(loginDTO);
            sessionStorage.setItem('token', user?.token || "");
            sessionStorage.setItem('user', String(user?.roles.includes(ERole.User)) || 'false');
            sessionStorage.setItem('admin', String(user?.roles.includes(ERole.Admin)) || 'false');
            sessionStorage.setItem('username', user?.username || "");
            sessionStorage.setItem('id', user?.id.toString() || "");
            return user;
        },
        async signup(context, signupDTO: ISignupDTO): Promise<void> {
            await AccountApi.signup(signupDTO);
        },
        async getUsers(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const users = await UserApi.getAll();
                context.commit('setUsers', users);
            }
        },
        async getUser(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const user = await UserApi.get(id);
                context.commit('setUser', user);
            }
        },
        async getLastUser(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const user = await UserApi.getLast();
                context.commit('setUser', user);
            }
        },
        async getUserByUsername(context, username: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const user = await UserApi.getByUsername(username);
                context.commit('setUser', user);
            }
        },
        async updateUser(context, user: IUpdateDTO): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await AccountApi.update(user);
            }
        },
        async deleteUser(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await UserApi.delete(id);
            }
        },
        async getRoles(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const roles = await RoleApi.getAll();
                context.commit('setRoles', roles);
            }
        },
        async getRole(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const role = await RoleApi.get(id);
                context.commit('setRole', role);
            }
        },
        async getRoleByName(context, name: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const role = await RoleApi.getByName(name);
                context.commit('setRole', role);
            }
        },
        async getUserOrganizations(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const userOrganizations = await UserOrganizationApi.getAll();
                context.commit('setUserOrganizations', userOrganizations);
            }
        },
        async getUserOrganizationsByUser(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const userOrganizations = await UserOrganizationApi.getAllByUser(id);
                context.commit('setUserOrganizations', userOrganizations);
            }
        },
        async getUserOrganizationsByOrganization(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const userOrganizations = await UserOrganizationApi.getAllByOrganization(id);
                context.commit('setUserOrganizations', userOrganizations);
            }
        },
        async getUserOrganization(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const userOrganization = await UserOrganizationApi.get(id);
                context.commit('setUserOrganization', userOrganization);
            }
        },
        async createUserOrganization(context, userOrganization: IUserOrganization): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await UserOrganizationApi.create(userOrganization);
            }
        },
        async updateUserOrganization(context, userOrganization: IUserOrganization): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await UserOrganizationApi.update(userOrganization);
            }
        },
        async deleteUserOrganization(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await UserOrganizationApi.delete(id);
            }
        },
        async getMeetings(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetings = await MeetingApi.getAll();
                context.commit('setMeetings', meetings);
            }
        },
        async getMeeting(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meeting = await MeetingApi.get(id);
                context.commit('setMeeting', meeting);
            }
        },
        async getLastMeeting(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meeting = await MeetingApi.getLast();
                context.commit('setMeeting', meeting);
            }
        },
        async createMeeting(context, meetingCreate: IMeetingCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingApi.create(meetingCreate);
            }
        },
        async updateMeeting(context, meeting: IMeeting): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingApi.update(meeting);
            }
        },
        async deleteMeeting(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingApi.delete(id);
            }
        },
        async getOrganizations(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const organizations = await OrganizationApi.getAll();
                context.commit('setOrganizations', organizations);
            }
        },
        async getOrganization(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const organization = await OrganizationApi.get(id);
                context.commit('setOrganization', organization);
            }
        },
        async getOrganizationByName(context, name: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const organization = await OrganizationApi.getByName(name);
                context.commit('setOrganization', organization);
            }
        },
        async createOrganization(context, organizationCreate: IOrganizationCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await OrganizationApi.create(organizationCreate);
            }
        },
        async updateOrganization(context, organization: IOrganization): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await OrganizationApi.update(organization);
            }
        },
        async deleteOrganization(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await OrganizationApi.delete(id);
            }
        },
        async getHolidaysAdmin(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const holidays = await HolidayApi.getAllAdmin();
                context.commit('setHolidays', holidays);
            }
        },
        async getHolidays(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const holidays = await HolidayApi.getAll();
                context.commit('setHolidays', holidays);
            }
        },
        async getHoliday(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const holiday = await HolidayApi.get(id);
                context.commit('setHoliday', holiday);
            }
        },
        async getLastHoliday(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const holiday = await HolidayApi.getLast();
                context.commit('setHoliday', holiday);
            }
        },
        async createHoliday(context, holidayCreate: IHolidayCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await HolidayApi.create(holidayCreate);
            }
        },
        async updateHoliday(context, holiday: IHoliday): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await HolidayApi.update(holiday);
            }
        },
        async deleteHoliday(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await HolidayApi.delete(id);
            }
        },
        async getOrganizationHolidays(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const organizationHolidays = await OrganizationHolidayApi.getAll();
                context.commit('setOrganizationHolidays', organizationHolidays);
            }
        },
        async getOrganizationHolidaysByHoliday(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const organizationHolidays = await OrganizationHolidayApi.getAllByHoliday(id);
                context.commit('setOrganizationHolidaysByHoliday', organizationHolidays);
            }
        },
        async getOrganizationHolidaysByOrganization(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const organizationHolidays = await OrganizationHolidayApi.getAllByOrganization(id);
                context.commit('setOrganizationHolidaysByOrganization', organizationHolidays);
            }
        },
        async getOrganizationHoliday(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const organizationHoliday = await OrganizationHolidayApi.get(id);
                context.commit('setOrganizationHoliday', organizationHoliday);
            }
        },
        async createOrganizationHoliday(context, organizationHolidayCreate: IOrganizationHolidayCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await OrganizationHolidayApi.create(organizationHolidayCreate);
            }
        },
        async updateOrganizationHoliday(context, organizationHoliday: IOrganizationHoliday): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await OrganizationHolidayApi.update(organizationHoliday);
            }
        },
        async deleteOrganizationHoliday(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await OrganizationHolidayApi.delete(id);
            }
        },
        async getQuestionnaires(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const questionnaires = await QuestionnaireApi.getAll();
                context.commit('setQuestionnaires', questionnaires);
            }
        },
        async getQuestionnaire(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const questionnaire = await QuestionnaireApi.get(id);
                context.commit('setQuestionnaire', questionnaire);
            }
        },
        async getLastQuestionnaire(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const questionnaire = await QuestionnaireApi.getLast();
                context.commit('setQuestionnaire', questionnaire);
            }
        },
        async getQuestionnaireByMeeting(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const questionnaire = await QuestionnaireApi.getByMeeting(id);
                context.commit('setQuestionnaire', questionnaire);
            }
        },
        async createQuestionnaire(context, questionnaireCreate: IQuestionnaireCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await QuestionnaireApi.create(questionnaireCreate);
            }
        },
        async updateQuestionnaire(context, questionnaire: IQuestionnaire): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await QuestionnaireApi.update(questionnaire);
            }
        },
        async deleteQuestionnaire(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await QuestionnaireApi.delete(id);
            }
        },
        async getMeetingTimes(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetingTimes = await MeetingTimeApi.getAll();
                context.commit('setMeetingTimes', meetingTimes);
            }
        },
        async getMeetingTimesByQuestionnaire(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetingTimes = await MeetingTimeApi.getAllByQuestionnaire(id);
                context.commit('setMeetingTimes', meetingTimes);
            }
        },
        async getMeetingTime(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetingTime = await MeetingTimeApi.get(id);
                context.commit('setMeetingTime', meetingTime);
            }
        },
        async createMeetingTime(context, meetingTime: IMeetingTimeCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingTimeApi.create(meetingTime);
            }
        },
        async updateMeetingTime(context, meetingTime: IMeetingTime): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingTimeApi.update(meetingTime);
            }
        },
        async deleteMeetingTime(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingTimeApi.delete(id);
            }
        },
        async getAnswers(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const answers = await AnswerApi.getAll();
                context.commit('setAnswers', answers);
            }
        },
        async getAnswersByQuestionnaire(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const answers = await AnswerApi.getAllByQuestionnaire(id);
                context.commit('setAnswers', answers);
            }
        },
        async getAnswer(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const answer = await AnswerApi.get(id);
                context.commit('setAnswer', answer);
            }
        },
        async createAnswer(context, answer: IAnswerCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await AnswerApi.create(answer);
            }
        },
        async updateAnswer(context, answer: IAnswer): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await AnswerApi.update(answer);
            }
        },
        async deleteAnswer(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await AnswerApi.delete(id);
            }
        },
        async getMemberAnswers(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const memberAnswers = await MemberAnswerApi.getAll();
                context.commit('setMemberAnswers', memberAnswers);
            }
        },
        async getMemberAnswersByMeeting(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const memberAnswers = await MemberAnswerApi.getAllByMeeting(id);
                context.commit('setMemberAnswers', memberAnswers);
            }
        },
        async getMemberAnswersByMeetingMember(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const memberAnswers = await MemberAnswerApi.getAllByMeetingMember(id);
                context.commit('setMemberAnswers', memberAnswers);
            }
        },
        async getMemberAnswersByMeetingAndUser(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const memberAnswers = await MemberAnswerApi.getAllByMeetingAndUser(id);
                context.commit('setMemberAnswers', memberAnswers);
            }
        },
        async getMemberAnswer(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const memberAnswer = await MemberAnswerApi.get(id);
                context.commit('setMemberAnswer', memberAnswer);
            }
        },
        async createMemberAnswer(context, memberAnswer: IMemberAnswerCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MemberAnswerApi.create(memberAnswer);
            }
        },
        async updateMemberAnswer(context, memberAnswer: IMemberAnswer): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MemberAnswerApi.update(memberAnswer);
            }
        },
        async deleteMemberAnswer(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MemberAnswerApi.delete(id);
            }
        },
        async getBuildings(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const buildings = await BuildingApi.getAll();
                context.commit('setBuildings', buildings);
            }
        },
        async getBuilding(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const building = await BuildingApi.get(id);
                context.commit('setBuilding', building);
            }
        },
        async getBuildingByName(context, name: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const building = await BuildingApi.getByName(name);
                context.commit('setBuilding', building);
            }
        },
        async createBuilding(context, building: IBuildingCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await BuildingApi.create(building);
            }
        },
        async updateBuilding(context, building: IBuilding): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await BuildingApi.update(building);
            }
        },
        async deleteBuilding(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await BuildingApi.delete(id);
            }
        },
        async getEquipments(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const equipments = await EquipmentApi.getAll();
                context.commit('setEquipments', equipments);
            }
        },
        async getEquipment(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const equipment = await EquipmentApi.get(id);
                context.commit('setEquipment', equipment);
            }
        },
        async getEquipmentByName(context, name: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const equipment = await EquipmentApi.getByName(name);
                context.commit('setEquipment', equipment);
            }
        },
        async createEquipment(context, equipment: IEquipmentCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await EquipmentApi.create(equipment);
            }
        },
        async updateEquipment(context, equipment: IEquipment): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await EquipmentApi.update(equipment);
            }
        },
        async deleteEquipment(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await EquipmentApi.delete(id);
            }
        },
        async getTalTechRoomResponses(context, query: ITalTechRoomsQuery): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const talTechRooms = await TalTechRoomsApi.requestAll(query);
                context.commit('setTalTechRoomResponses', talTechRooms);
            }
        },
        async bookTalTechRoomResponse(context, query: ITalTechRoomsBookQuery): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const talTechRoom = await TalTechRoomsApi.book(query);
                context.commit('setTalTechRoomResponse', talTechRoom);
            }
        },
        async getTalTechRoomsByMeeting(context, meetingId: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const talTechRooms = await TalTechRoomsApi.getAllByMeeting(meetingId);
                context.commit('setTalTechRooms', talTechRooms);
            }
        },
        async getTalTechRoom(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const talTechRoom = await TalTechRoomsApi.get(id);
                context.commit('setTalTechRoom', talTechRoom);
            }
        },

        async getLastTalTechRoom(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const talTechRoom = await TalTechRoomsApi.getLast();
                context.commit('setTalTechRoom', talTechRoom);
            }
        },
        async createTalTechRoom(context, talTechRoom: ITalTechRoomCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await TalTechRoomsApi.create(talTechRoom);
            }
        },
        async deleteTalTechRoom(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await TalTechRoomsApi.delete(id);
            }
        },
        async getBookTypes(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const bookTypes = await BookTypeApi.getAll();
                context.commit('setBookTypes', bookTypes);
            }
        },
        async getBookType(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const bookType = await BookTypeApi.get(id);
                context.commit('setBookType', bookType);
            }
        },
        async getMeetingMembers(context): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetingMembers = await MeetingMemberApi.getAll();
                context.commit('setMeetingMembers', meetingMembers);
            }
        },
        async getMeetingMembersByMeeting(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetingMembers = await MeetingMemberApi.getAllByMeeting(id);
                context.commit('setMeetingMembers', meetingMembers);
            }
        },
        async getMeetingMembersByUser(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetingMembers = await MeetingMemberApi.getAllByUser(id);
                context.commit('setMeetingMembers', meetingMembers);
            }
        },
        async getMeetingMember(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetingMember = await MeetingMemberApi.get(id);
                context.commit('setMeetingMember', meetingMember);
            }
        },
        async getMeetingMemberByMeetingAndUser(context, questionnaireId) : Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                const meetingMember = await MeetingMemberApi.getByMeetingAndUser(questionnaireId);
                context.commit('setMeetingMember', meetingMember);
            }
        },
        async createMeetingMember(context, meetingMember: IMeetingMemberCreate): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingMemberApi.create(meetingMember);
            }
        },
        async updateMeetingMember(context, meetingMember: IMeetingMember): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingMemberApi.update(meetingMember);
            }
        },
        async deleteMeetingMember(context, id: string): Promise<void> {
            if (sessionStorage.getItem('token') !== null && sessionStorage.getItem('token') !== "") {
                await MeetingMemberApi.delete(id);
            }
        }
    },
    modules: {
    }
})
