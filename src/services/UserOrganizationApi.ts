import { IUserOrganization, IUserOrganizationCreate } from '@/domain/IUserOrganization';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class UserOrganizationApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/user_organizations",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IUserOrganization[]> {
        const url = "";
        try {
            const response = await this.axios.get<IUserOrganization[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByUser(userId: string): Promise<IUserOrganization[]> {
        const url = "user/" + userId;
        try {
            const response = await this.axios.get<IUserOrganization[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByOrganization(organizationId: string): Promise<IUserOrganization[]> {
        const url = "organization/" + organizationId;
        try {
            const response = await this.axios.get<IUserOrganization[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IUserOrganization|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IUserOrganization>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(userOrganization: IUserOrganizationCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IUserOrganizationCreate>(url, userOrganization, { headers: authHeader() });
        } catch (error) {

        }
    }

    static async update(userOrganization: IUserOrganization): Promise<void> {
        const url = "/" + userOrganization.id;
        try {
            const response = await this.axios.post<IUserOrganization>(url, userOrganization, { headers: authHeader() });
        } catch (error) {

        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IUserOrganization>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
