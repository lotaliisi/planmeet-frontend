import { IMemberAnswer, IMemberAnswerCreate } from '@/domain/IMemberAnswer';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class MemberAnswerApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/member_answers",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IMemberAnswer[]> {
        const url = "";
        try {
            const response = await this.axios.get<IMemberAnswer[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByMeeting(meetingId: string): Promise<IMemberAnswer[]> {
        const url = "/meeting/" + meetingId;
        try {
            const response = await this.axios.get<IMemberAnswer[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByMeetingMember(meetingMemberId: string): Promise<IMemberAnswer[]> {
        const url = "/user/" + meetingMemberId;
        try {
            const response = await this.axios.get<IMemberAnswer[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByMeetingAndUser(meetingId: string): Promise<IMemberAnswer[]> {
        const url = "/user/meeting/" + meetingId + "/";
        try {
            const response = await this.axios.get<IMemberAnswer[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IMemberAnswer|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IMemberAnswer>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(memberAnswer: IMemberAnswerCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IMemberAnswerCreate>(url, memberAnswer, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(memberAnswer: IMemberAnswer): Promise<void> {
        const url = "/" + memberAnswer.id;
        try {
            const response = await this.axios.post<IMemberAnswer>(url, memberAnswer, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IMemberAnswer>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
