import { IRole } from '@/domain/IRole';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class RoleApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/roles",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IRole[]> {
        const url = "";
        try {
            const response = await this.axios.get<IRole[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IRole|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IRole>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getByName(name: string): Promise<IRole|null> {
        const url = "/name/" + name;
        try {
            const response = await this.axios.get<IRole>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }
}
