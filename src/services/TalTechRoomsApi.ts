import { ITalTechRoom, ITalTechRoomCreate } from '@/domain/ITalTechRoom';
import { ITalTechRoomsBookQuery } from '@/domain/ITalTechRoomsBookQuery';
import { ITalTechRoomsQuery } from '@/domain/ITalTechRoomsQuery';
import { ITalTechRoomsResponse } from '@/domain/ITalTechRoomsResponse';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class TalTechRoomsApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/taltech",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async requestAll(query: ITalTechRoomsQuery): Promise<ITalTechRoomsResponse[]> {
        const url = "/find";
        try {
            const response = await this.axios.post<ITalTechRoomsResponse[]>(url, query, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async book(query: ITalTechRoomsBookQuery): Promise<ITalTechRoomsResponse | null> {
        const url = "/book";
        try {
            const response = await this.axios.post<ITalTechRoomsResponse>(url, query, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getAllByMeeting(meetingId: string): Promise<ITalTechRoom[]> {
        const url = "/meeting/" + meetingId;
        try {
            const response = await this.axios.get<ITalTechRoom[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<ITalTechRoom|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<ITalTechRoom>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getLast(): Promise<ITalTechRoom|null> {
        const url = "/last";
        try {
            const response = await this.axios.get<ITalTechRoom>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(talTechRoom: ITalTechRoomCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<ITalTechRoomCreate>(url, talTechRoom, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<ITalTechRoom>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
