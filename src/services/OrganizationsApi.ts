import { IOrganization, IOrganizationCreate } from '@/domain/IOrganization';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class OrganizationApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/organizations",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IOrganization[]> {
        const url = "";
        try {
            const response = await this.axios.get<IOrganization[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IOrganization|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IOrganization>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getByName(name: string): Promise<IOrganization|null> {
        const url = "/name/" + name;
        try {
            const response = await this.axios.get<IOrganization>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(organization: IOrganizationCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IOrganizationCreate>(url, organization, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(organization: IOrganization): Promise<void> {
        const url = "/" + organization.id;
        try {
            const response = await this.axios.post<IOrganization>(url, organization, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IOrganization>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
