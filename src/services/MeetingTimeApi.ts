import { IMeetingTime, IMeetingTimeCreate } from '@/domain/IMeetingTime';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class MeetingTimeApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/meeting_times",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IMeetingTime[]> {
        const url = "";
        try {
            const response = await this.axios.get<IMeetingTime[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByQuestionnaire(questionnaireId: string): Promise<IMeetingTime[]> {
        const url = "/questionnaire/" + questionnaireId;
        try {
            const response = await this.axios.get<IMeetingTime[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IMeetingTime|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IMeetingTime>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(meetingTime: IMeetingTimeCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IMeetingTimeCreate>(url, meetingTime, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(meetingTime: IMeetingTime): Promise<void> {
        const url = "/" + meetingTime.id;
        try {
            const response = await this.axios.post<IMeetingTime>(url, meetingTime, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IMeetingTime>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
