export default function authHeader(): { Authorization: string; } | unknown {
    const user = sessionStorage.getItem('token');
    if (user) {
        return { Authorization: 'Bearer ' + user };
    } else {
        return {};
    }
}
