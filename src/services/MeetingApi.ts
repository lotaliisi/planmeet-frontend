import { IMeeting, IMeetingCreate } from '@/domain/IMeeting';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class MeetingApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/meetings",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IMeeting[]> {
        const url = "";
        try {
            const response = await this.axios.get<IMeeting[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IMeeting|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IMeeting>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getLast(): Promise<IMeeting|null> {
        const url = "/last";
        try {
            const response = await this.axios.get<IMeeting>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(meeting: IMeetingCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IMeetingCreate>(url, meeting, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(meeting: IMeeting): Promise<void> {
        const url = "/" + meeting.id;
        try {
            const response = await this.axios.post<IMeeting>(url, meeting, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IMeeting>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
