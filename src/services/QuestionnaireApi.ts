import { IQuestionnaire, IQuestionnaireCreate } from '@/domain/IQuestionnaire';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class QuestionnaireApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/questionnaires",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IQuestionnaire[]> {
        const url = "";
        try {
            const response = await this.axios.get<IQuestionnaire[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IQuestionnaire|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IQuestionnaire>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getLast(): Promise<IQuestionnaire|null> {
        const url = "/last";
        try {
            const response = await this.axios.get<IQuestionnaire>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getByMeeting(id: string): Promise<IQuestionnaire|null> {
        const url = "/meeting/" + id;
        try {
            const response = await this.axios.get<IQuestionnaire>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(questionnaire: IQuestionnaireCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IQuestionnaireCreate>(url, questionnaire, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(questionnaire: IQuestionnaire): Promise<void> {
        const url = "/" + questionnaire.id;
        try {
            const response = await this.axios.post<IQuestionnaire>(url, questionnaire, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IQuestionnaire>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
