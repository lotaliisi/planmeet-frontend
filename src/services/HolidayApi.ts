import { IHoliday, IHolidayCreate } from '@/domain/IHoliday';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class HolidayApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/holidays",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAllAdmin(): Promise<IHoliday[]> {
        const url = "/admin";
        try {
            const response = await this.axios.get<IHoliday[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAll(): Promise<IHoliday[]> {
        const url = "";
        try {
            const response = await this.axios.get<IHoliday[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IHoliday|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IHoliday>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getLast(): Promise<IHoliday|null> {
        const url = "/last";
        try {
            const response = await this.axios.get<IHoliday>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(holiday: IHolidayCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IHolidayCreate>(url, holiday, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(holiday: IHoliday): Promise<void> {
        const url = "/" + holiday.id;
        try {
            const response = await this.axios.post<IHoliday>(url, holiday, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IHoliday>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
