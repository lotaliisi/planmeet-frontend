import { ILoginDTO } from './../types/ILoginDTO';
import Axios from 'axios';
import { ISignupDTO } from '@/types/ISignupDTO';
import { ISignupResponseDTO } from '@/types/ISignupResponseDTO';
import { ILoginResponseDTO } from '@/types/ILoginResponseDTO';
import { IUpdateDTO } from '@/types/IUpdateDTO';
import authHeader from './AuthHeader';

export abstract class AccountApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/api/auth/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async signin(loginDTO: ILoginDTO): Promise<ILoginResponseDTO|null> {
        const url = "signin";
        try {
            const response = await this.axios.post<ILoginResponseDTO>(url, loginDTO);
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async signup(signupDTO: ISignupDTO): Promise<ISignupResponseDTO|null> {
        const url = "signup";
        try {
            const response = await this.axios.post<ISignupResponseDTO>(url, signupDTO);
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async update(user: IUpdateDTO): Promise<void> {
        const url = "update";
        try {
            await this.axios.post<IUpdateDTO>(url, user, { headers: authHeader() });
        } catch (error) {
        }
    }
}
