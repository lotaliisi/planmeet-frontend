import { IUser } from '@/domain/IUser';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class UserApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/users",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IUser[]> {
        const url = "";
        try {
            const response = await this.axios.get<IUser[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IUser|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IUser>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getLast(): Promise<IUser|null> {
        const url = "/last";
        try {
            const response = await this.axios.get<IUser>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getByUsername(username: string): Promise<IUser|null> {
        const url = "/username/" + username;
        try {
            const response = await this.axios.get<IUser>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IUser>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
