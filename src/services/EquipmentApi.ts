import { IEquipment, IEquipmentCreate } from '@/domain/IEquipment';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class EquipmentApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/equipments",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IEquipment[]> {
        const url = "";
        try {
            const response = await this.axios.get<IEquipment[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IEquipment|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IEquipment>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async getByName(name: string): Promise<IEquipment|null> {
        const url = "/name/" + name;
        try {
            const response = await this.axios.get<IEquipment>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(equipment: IEquipmentCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IEquipmentCreate>(url, equipment, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(equipment: IEquipment): Promise<void> {
        const url = "/" + equipment.id;
        try {
            const response = await this.axios.post<IEquipment>(url, equipment, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IEquipment>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
