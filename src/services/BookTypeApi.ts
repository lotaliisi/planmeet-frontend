import { IBookType } from '@/domain/IBookType';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class BookTypeApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/book_types",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IBookType[]> {
        const url = "";
        try {
            const response = await this.axios.get<IBookType[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IBookType|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IBookType>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }
}
