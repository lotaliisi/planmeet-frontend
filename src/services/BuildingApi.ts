import { IBuilding, IBuildingCreate } from '@/domain/IBuilding';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class BuildingApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/buildings",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IBuilding[]> {
        const url = "";
        try {
            const response = await this.axios.get<IBuilding[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getByName(name: string): Promise<IBuilding|null> {
        const url = "/name/" + name;
        try {
            const response = await this.axios.get<IBuilding>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async get(id: string): Promise<IBuilding|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IBuilding>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(building: IBuildingCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IBuildingCreate>(url, building, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(building: IBuilding): Promise<void> {
        const url = "/" + building.id;
        try {
            const response = await this.axios.post<IBuilding>(url, building, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IBuilding>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
