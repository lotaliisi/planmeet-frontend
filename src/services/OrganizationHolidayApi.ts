import { IOrganizationHoliday, IOrganizationHolidayCreate } from '@/domain/IOrganizationHoliday';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class OrganizationHolidayApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/organization_holidays",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IOrganizationHoliday[]> {
        const url = "";
        try {
            const response = await this.axios.get<IOrganizationHoliday[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByHoliday(holidayId: string): Promise<IOrganizationHoliday[]> {
        const url = "holiday/" + holidayId;
        try {
            const response = await this.axios.get<IOrganizationHoliday[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByOrganization(organizationId: string): Promise<IOrganizationHoliday[]> {
        const url = "organization/" + organizationId;
        try {
            const response = await this.axios.get<IOrganizationHoliday[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IOrganizationHoliday|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IOrganizationHoliday>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(organizationHoliday: IOrganizationHolidayCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IOrganizationHolidayCreate>(url, organizationHoliday, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(organizationHoliday: IOrganizationHoliday): Promise<void> {
        const url = "/" + organizationHoliday.id;
        try {
            const response = await this.axios.post<IOrganizationHoliday>(url, organizationHoliday, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IOrganizationHoliday>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
