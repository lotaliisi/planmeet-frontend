import { IMeetingMember, IMeetingMemberCreate } from '@/domain/IMeetingMember';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class MeetingMemberApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/meeting_members",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IMeetingMember[]> {
        const url = "";
        try {
            const response = await this.axios.get<IMeetingMember[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByMeeting(meetingId: string): Promise<IMeetingMember[]> {
        const url = "/meeting/" + meetingId;
        try {
            const response = await this.axios.get<IMeetingMember[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByUser(userId: string): Promise<IMeetingMember[]> {
        const url = "/user/" + userId;
        try {
            const response = await this.axios.get<IMeetingMember[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getByMeetingAndUser(meetingId: string):
    Promise<IMeetingMember|null> {
        const url = "/user/meeting/" + meetingId;
        try {
            const response = await this.axios.get<IMeetingMember>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async get(id: string): Promise<IMeetingMember|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IMeetingMember>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(meetingMember: IMeetingMemberCreate): Promise<void> {
        const url = "";
        try {
            const response = await this.axios.post<IMeetingMemberCreate>(url, meetingMember, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(meetingMember: IMeetingMember): Promise<void> {
        const url = "/" + meetingMember.id;
        try {
            const response = await this.axios.post<IMeetingMember>(url, meetingMember, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IMeetingMember>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
