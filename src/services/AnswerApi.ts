import { IAnswer, IAnswerCreate } from '@/domain/IAnswer';
import Axios from 'axios';
import authHeader from './AuthHeader';

export abstract class AnswerApi {
    private static axios = Axios.create(
        {
            baseURL: process.env.VUE_APP_BASE_URL + "/answers",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IAnswer[]> {
        const url = "";
        try {
            const response = await this.axios.get<IAnswer[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async getAllByQuestionnaire(questionnaireId: string): Promise<IAnswer[]> {
        const url = "/questionnaire/" + questionnaireId;
        try {
            const response = await this.axios.get<IAnswer[]>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            return [];
        }
    }

    static async get(id: string): Promise<IAnswer|null> {
        const url = "/" + id;
        try {
            const response = await this.axios.get<IAnswer>(url, { headers: authHeader() });
            if (response.status === 200) {
                return response.data;
            }
            return null;
        } catch (error) {
            return null;
        }
    }

    static async create(answer: IAnswerCreate): Promise<void> {
        const url = "";
        try {
            await this.axios.post<IAnswerCreate>(url, answer, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async update(answer: IAnswer): Promise<void> {
        const url = "/" + answer.id;
        try {
            await this.axios.post<IAnswer>(url, answer, { headers: authHeader() });
        } catch (error) {
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "/" + id;
        try {
            const response = await this.axios.delete<IAnswer>(url, { headers: authHeader() });
            if (response.status === 200) {
            }
        } catch (error) {
        }
    }
}
