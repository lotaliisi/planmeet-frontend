import { IMeeting } from "./IMeeting";
import { IUser } from "./IUser";

export interface IMeetingMember {
    id: number;
    organizer: boolean;
    meeting: IMeeting | null;
    user: IUser | null;
    answersCount: number;
}

export interface IMeetingMemberCreate {
    organizer: boolean;
    meeting: IMeeting | null;
    user: IUser | null;
}