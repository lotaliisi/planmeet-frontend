import { IMeeting } from "./IMeeting";

export interface IQuestionnaire {
    id: number;
    deadline: string;
    meeting: IMeeting | null;
}

export interface IQuestionnaireCreate {
    deadline: string;
    meeting: IMeeting | null;
}