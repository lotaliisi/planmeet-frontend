export interface IBuilding {
    id: number;
    code: string;
    name: string;
}

export interface IBuildingCreate {
    code: string;
    name: string;
}