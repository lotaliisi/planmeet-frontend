export interface IBookType {
    id: number;
    code: String;
    name: String;
}