import { IHoliday } from "./IHoliday";
import { IOrganization } from "./IOrganization";

export interface IOrganizationHoliday {
    id: number;
    organization: IOrganization | null;
    holiday: IHoliday | null;
}

export interface IOrganizationHolidayCreate {
    organization: IOrganization | null;
    holiday: IHoliday | null;
}