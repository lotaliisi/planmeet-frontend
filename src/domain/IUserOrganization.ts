import { IOrganization } from "./IOrganization";
import { IUser } from "./IUser";

export interface IUserOrganization {
    id: number;
    user: IUser | null;
    organization: IOrganization | null;
}

export interface IUserOrganizationCreate {
    user: IUser | null;
    organization: IOrganization | null;
}