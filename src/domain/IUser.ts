import { IRole } from "./IRole";
import { IUserOrganization } from "./IUserOrganization";

export interface IUser {
    id: number;
    username: string;
    email: string;
    firstName: string;
    lastName: string;
    birthday: string;
    password: string;
    roles: IRole[];
    organizations: IUserOrganization[];
}

