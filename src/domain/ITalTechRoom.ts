import { IMeeting } from "./IMeeting";

export interface ITalTechRoom {
    id: number;
    row_num: string;
    hoone: string;
    ruum_id: string;
    ruum_kood: string;
    hoone_nimetus: string;
    korrus: string;
    avalik: string;
    kohtade_arv: string;
    vaba: string;
    vaba_txt: string;
    ruumi_tyyp: string;
    bron_txt: string;
    varustus: string;
    meeting: IMeeting | null;
}

export interface ITalTechRoomCreate {
    row_num: string;
    hoone: string;
    ruum_id: string;
    ruum_kood: string;
    hoone_nimetus: string;
    korrus: string;
    avalik: string;
    kohtade_arv: string;
    vaba: string;
    vaba_txt: string;
    ruumi_tyyp: string;
    bron_txt: string;
    varustus: string;
    meeting: IMeeting | null;
}