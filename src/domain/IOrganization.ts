export interface IOrganization {
    id: number;
    name: string;
}

export interface IOrganizationCreate {
    name: string;
}