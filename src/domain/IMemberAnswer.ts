import { IAnswer } from "./IAnswer";
import { IMeetingMember } from "./IMeetingMember";
import { IMeetingTime } from "./IMeetingTime";

export interface IMemberAnswer {
    id: number;
    meetingTime: IMeetingTime | null;
    meetingMember: IMeetingMember | null;
    answer: IAnswer | null;
}

export interface IMemberAnswerCreate {
    meetingTime: IMeetingTime | null;
    meetingMember: IMeetingMember | null;
    answer: IAnswer | null;
}