import { IQuestionnaire } from "./IQuestionnaire";

export interface IAnswer {
    id: number;
    name: string;
    questionnaire: IQuestionnaire | null;
}

export interface IAnswerCreate {
    name: string;
    questionnaire: IQuestionnaire | null;
}