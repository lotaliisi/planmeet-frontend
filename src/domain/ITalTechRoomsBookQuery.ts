import { ITalTechRoom } from "./ITalTechRoom";

export interface ITalTechRoomsBookQuery {
    times: string[];
    room: ITalTechRoom | null;
    code: string; 
    comment: string;
    personCode: string;
}