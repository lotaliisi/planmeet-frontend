export interface IMeeting {
    id: number;
    name: string;
    description: string;
    location: string;
    membersCount: number;
    startTime: Date | null;
    endTime: Date | null;
    timesCount: number;
}

export interface IMeetingCreate {
    name: string;
    description: string;
    location: string;
    startTime: Date | null;
    endTime: Date | null;
}
