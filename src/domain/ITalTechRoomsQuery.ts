export interface ITalTechRoomsQuery {
    times: string[];
    rooms: string[];
    buildings: string[];
    equipments: string[]; 
    min: string;
    max: string;
    free: boolean;
    publicUse: boolean;
}