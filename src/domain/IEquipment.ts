export interface IEquipment {
    id: number;
    name: string;
}

export interface IEquipmentCreate {
    name: string;
}