import { IQuestionnaire } from "./IQuestionnaire";

export interface IMeetingTime {
    id: number;
    startTime: Date | null;
    endTime: Date | null;
    questionnaire: IQuestionnaire | null;
    answersCount: number;
}

export interface IMeetingTimeCreate {
    startTime: Date | null;
    endTime: Date | null;
    questionnaire: IQuestionnaire | null;
}