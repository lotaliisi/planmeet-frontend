export interface IHoliday {
    id: number;
    name: string;
    vocation: boolean;
    date: string;
}

export interface IHolidayCreate {
    name: string;
    vocation: boolean;
    date: string;
}