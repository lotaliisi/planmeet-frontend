import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { extend, setInteractionMode, ValidationProvider, ValidationObserver } from 'vee-validate';
import { email, max, required } from 'vee-validate/dist/rules'

import {
    faHome,
    faUser,
    faUserPlus,
    faSignInAlt,
    faSignOutAlt,
    faCalendar,
    faBars,
    faGraduationCap,
    faTrashAlt,
    faPencilAlt,
    faArrowLeft,
    faArrowRight,
    faBirthdayCake,
    faCheck,
    faTimes,
    faPlay,
    faQuestion,
    faCheckSquare,
    faUniversity,
    faUsers
} from '@fortawesome/free-solid-svg-icons';
import './assets/main.css';

library.add(
    faHome,
    faUser,
    faUserPlus,
    faSignInAlt,
    faSignOutAlt,
    faCalendar,
    faBars,
    faGraduationCap,
    faTrashAlt,
    faPencilAlt,
    faArrowLeft,
    faArrowRight,
    faBirthdayCake,
    faCheck,
    faTimes,
    faPlay,
    faQuestion,
    faCheckSquare,
    faUniversity,
    faUsers
);

Vue.config.productionTip = false

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
Vue.component('extend', extend);
Vue.component('setInteractionMode', setInteractionMode);

extend('required', {
    ...required,
    message: 'Väli ei tohi olla tühi'
})

extend('max', {
    ...max,
    message: '{_field_} ei tohi olla pikem kui {length}'
})

extend('email', {
    ...email,
    message: 'Sisesta korrektne email'
})

extend('dayhours', value => {
    const hours = new Date("2000-01-01 " + value).getHours();
    if (hours >= 8 && hours <= 22) {
        return true;
    }
    return 'Kellaaeg peab olema vahemikus 8-22';
});

extend('startEnd', {
    validate(value, { startDate, startTime, endDate, endTime }: Record<string, any>) {
        if (!startDate) startDate = "2000-01-01";
        if (!endDate) endDate = "9999-12-12";
        if (!startTime) startTime = "00:00";
        if (!endTime) startTime = "23:59";
        const start = new Date(startDate + " " + startTime);
        const end = new Date(endDate + " " + endTime);
        if (start < end) {
            return true;
        }
        return 'Alguse aeg peab olema enne lõpu aega';
    },
    params: ['startDate', 'startTime', 'endDate', 'endTime']
});

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
