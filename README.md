# PlanMeet frontend

PlanMeet is application for TalTech student organizations to plan their meetings. It was created as final bachelor's thesis project for Tallinn University of Technology. Application contains of two parts - backend and frontend.

Backend can be found in: https://bitbucket.org/lotaliisi/planmeet-backend

Frontend can be found in: https://bitbucket.org/lotaliisi/planmeet-frontend

## Project technologies in frontend

- TypeScript, HTML, CSS as programming languages.
- Vue as main framework.
- VueBootstrap to help with user interface.

## Running PlanMeet application
PlanMeet application needs frontend, backend and database to work. Instructions to set up frontend can be found here and instructions to set up backend and database can be found in [PlanMeet backend](https://bitbucket.org/lotaliisi/planmeet-backend).

Before running frontend project check backend url in file .env.development to be the same as where your backend project runs.

Below you can find useful commands for PlanMeet frontend project.
### Downloading project from Bitbucket
```
git clone https://lotaliisi@bitbucket.org/lotaliisi/planmeet-frontend.git
```
### Setup - installs needed packages
```
npm install
```
### Running - compiles and starts development server
```
npm run serve
```

### Production - compiles and minifies project to be runned in production into dist directory
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

## Logging in to PlanMeet

When application is running then it is possible to log in with credentials below:

For user:

- username: user
- password: test

For admin:

- username: admin
- password: test
